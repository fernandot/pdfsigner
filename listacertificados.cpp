#include "listacertificados.h"
#include "ui_listacertificados.h"
#include <QMessageBox>

ListaCertificados::ListaCertificados(QWidget *parent, HPWincrypt *crypt, const QString &file, const QString &saveFile, const QString &reason, const QString &local) :
    QDialog(parent),
    ui(new Ui::ListaCertificados),
    file(file),
    saveFile(saveFile),
    reason(reason),
    local(local)
{
    ui->setupUi(this);

    this->crypt = crypt;
    if(crypt)
    {
        foreach (std::string str, crypt->getSignerStringList()) {
            ui->listWidget->addItem(str.data());
        }
    }
}

ListaCertificados::~ListaCertificados()
{
    delete ui;
}

void ListaCertificados::on_pushButton_clicked()
{
    if(!crypt || !ui->listWidget->selectedItems().size()) return;

    if(!crypt->CertDeleteCertificateFromStore(crypt->getSignerCertificate(ui->listWidget->selectedItems()[0]->text().toStdWString().data())))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("Não foi possível remover o certificado ")+ui->listWidget->selectedItems()[0]->text() + ".");
        msgBox.exec();
    }
    else
    {
        ui->listWidget->clear();
        foreach (std::string str, crypt->getSignerStringList()) {
            ui->listWidget->addItem(str.data());
        }
    }
}

void ListaCertificados::on_buttonBox_accepted()
{
    sign();
}

void ListaCertificados::on_listWidget_doubleClicked(const QModelIndex &index)
{
    this->close();
    sign();
}

void ListaCertificados::sign()
{
    if(!crypt)
    {
        QMessageBox::warning(this,"Erro","Não foi possível iniciar a criptografia.");
        return;
    }
    if(!ui->listWidget->selectedItems().size())
    {
        QMessageBox::warning(this,"Erro","Selecione um certificado.");
        return;
    }
    if(!file.size())
    {
        QMessageBox::warning(this,"Erro","Selecione um arquivo.");
        return;
    }
    if(!saveFile.size())
    {
        QMessageBox::warning(this,"Erro","Informe como salvar o arquivo.");
        return;
    }

    PCCERT_CONTEXT context = crypt->getSignerCertificate(ui->listWidget->selectedItems()[0]->text().toStdWString().data());
    HCRYPTPROV_OR_NCRYPT_KEY_HANDLE pk = crypt->getPrivateKey(context);
    if(!pk)
    {
        QMessageBox::warning(this,"Erro","Não foi possível assinar o arquivo, verifique se o Certificado está ativo.");
        return;
    }
    crypt->signData(context,pk,file.toStdString(),saveFile.toStdString(), reason.toStdString(), local.toStdString());
}
