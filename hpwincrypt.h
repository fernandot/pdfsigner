#ifndef HPWINCRYPT_H
#define HPWINCRYPT_H

#include <windows.h>
#include <wincrypt.h>
#include <deque>
#include <string>
#include <sstream>

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)

#define _Inout_
#define _Out_opt_



typedef ULONG_PTR HCRYPTPROV_OR_NCRYPT_KEY_HANDLE;

typedef ULONG_PTR HCRYPTPROV_LEGACY;

typedef HCERTSTORE (CALLBACK* CertOpenStoreType)(_In_       LPCSTR            lpszStoreProvider,
                                                 _In_       DWORD             dwMsgAndCertEncodingType,
                                                 _In_       HCRYPTPROV_LEGACY hCryptProv,
                                                 _In_       DWORD             dwFlags,
                                                 _In_ const void              *pvPara);

typedef PCCERT_CONTEXT (CALLBACK* CertFindCertificateInStoreType)(_In_       HCERTSTORE     hCertStore,
                                                                  _In_       DWORD          dwCertEncodingType,
                                                                  _In_       DWORD          dwFindFlags,
                                                                  _In_       DWORD          dwFindType,
                                                                  _In_ const void           *pvFindPara,
                                                                  _In_       PCCERT_CONTEXT pPrevCertContext);

typedef PCCERT_CONTEXT (CALLBACK* CertGetIssuerCertificateFromStoreType)(
  _In_     HCERTSTORE     hCertStore,
  _In_     PCCERT_CONTEXT pSubjectContext,
  _In_opt_ PCCERT_CONTEXT pPrevIssuerContext,
  _Inout_  DWORD          *pdwFlags
);

typedef PCCERT_CONTEXT (CALLBACK* CertEnumCertificatesInStoreType)(_In_ HCERTSTORE     hCertStore,
                                                               _In_ PCCERT_CONTEXT pPrevCertContext);

typedef BOOL (CALLBACK* CertDeleteCertificateFromStoreType)(
  _In_ PCCERT_CONTEXT pCertContext
);

typedef BOOL (CALLBACK* CertCloseStoreType)(_In_ HCERTSTORE hCertStore,
  _In_ DWORD      dwFlags
);

typedef BOOL (CALLBACK* CryptSignMessageType)(_In_          PCRYPT_SIGN_MESSAGE_PARA pSignPara,
  _In_          BOOL                     fDetachedSignature,
  _In_          DWORD                    cToBeSigned,
  _In_    const BYTE                     *rgpbToBeSigned[],
  _In_          DWORD                    rgcbToBeSigned[],
  _Out_         BYTE                     *pbSignedBlob,
  _Inout_       DWORD                    *pcbSignedBlob
);

typedef BOOL (CALLBACK* CryptVerifyMessageSignatureType)(
  _In_            PCRYPT_VERIFY_MESSAGE_PARA pVerifyPara,
  _In_            DWORD                      dwSignerIndex,
  _In_      const BYTE                       *pbSignedBlob,
  _In_            DWORD                      cbSignedBlob,
  _Out_           BYTE                       *pbDecoded,
  _Inout_         DWORD                      *pcbDecoded,
  _Out_opt_       PCCERT_CONTEXT             *ppSignerCert
);

typedef BOOL (CALLBACK* CryptAcquireCertificatePrivateKeyType)(
  _In_     PCCERT_CONTEXT                  pCert,
  _In_     DWORD                           dwFlags,
  _In_opt_ void                            *pvParameters,
  _Out_    HCRYPTPROV_OR_NCRYPT_KEY_HANDLE *phCryptProvOrNCryptKey,
  _Out_    DWORD                           *pdwKeySpec,
  _Out_    BOOL                            *pfCallerFreeProvOrNCryptKey
);

typedef BOOL (CALLBACK* CryptAcquireContextWType)(
  _Out_ HCRYPTPROV *phProv,
  _In_  LPCTSTR    pszContainer,
  _In_  LPCTSTR    pszProvider,
  _In_  DWORD      dwProvType,
  _In_  DWORD      dwFlags
);

typedef DWORD (CALLBACK*  CertGetNameStringWType)(
  _In_  PCCERT_CONTEXT pCertContext,
  _In_  DWORD          dwType,
  _In_  DWORD          dwFlags,
  _In_  void           *pvTypePara,
  _Out_ LPTSTR         pszNameString,
  _In_  DWORD          cchNameString
);

typedef BOOL (CALLBACK* CryptGetUserKeyType)(
  _In_  HCRYPTPROV hProv,
  _In_  DWORD      dwKeySpec,
  _Out_ HCRYPTKEY  *phUserKey
);

typedef BOOL (CALLBACK* CryptExportKeyType)(
  _In_    HCRYPTKEY hKey,
  _In_    HCRYPTKEY hExpKey,
  _In_    DWORD     dwBlobType,
  _In_    DWORD     dwFlags,
  _Out_   BYTE      *pbData,
  _Inout_ DWORD     *pdwDataLen
);

typedef BOOL (CALLBACK* CryptCreateHashType)(
  _In_  HCRYPTPROV hProv,
  _In_  ALG_ID     Algid,
  _In_  HCRYPTKEY  hKey,
  _In_  DWORD      dwFlags,
  _Out_ HCRYPTHASH *phHash
);

typedef BOOL (CALLBACK* CryptHashDataType)(
  _In_ HCRYPTHASH hHash,
  _In_ BYTE       *pbData,
  _In_ DWORD      dwDataLen,
  _In_ DWORD      dwFlags
);

typedef BOOL (CALLBACK* CryptSignHashWType)(
  _In_    HCRYPTHASH hHash,
  _In_    DWORD      dwKeySpec,
  _In_    LPCTSTR    sDescription,
  _In_    DWORD      dwFlags,
  _Out_   BYTE       *pbSignature,
  _Inout_ DWORD      *pdwSigLen
);

typedef BOOL (CALLBACK* CryptGetHashParamType)(
  _In_    HCRYPTHASH hHash,
  _In_    DWORD      dwParam,
  _Out_   BYTE       *pbData,
  _Inout_ DWORD      *pdwDataLen,
  _In_    DWORD      dwFlags
);

typedef BOOL (CALLBACK* CryptReleaseContextType)(
  _In_ HCRYPTPROV hProv,
  _In_ DWORD      dwFlags
);

typedef BOOL (CALLBACK* CryptDestroyHashType)(
  _In_ HCRYPTHASH hHash
);

class HPWincrypt
{
public:
    HPWincrypt();
    ~HPWincrypt();
    bool Init();

    HCERTSTORE getCertStore(const wchar_t *certName);
    std::deque<PCCERT_CONTEXT> &getSignerList();
    std::deque<std::string> &getSignerStringList();
    PCCERT_CONTEXT &getSignerCertificate(const wchar_t *subjectName);
    CRYPT_DATA_BLOB &getSignedMessage(PCCERT_CONTEXT signer, BYTE *msg, int msgLen);
    CRYPT_DATA_BLOB &getVerifiedSignedMessage(CRYPT_DATA_BLOB &pSignedMessageBlob);
    HCRYPTPROV_OR_NCRYPT_KEY_HANDLE &getPrivateKey(PCCERT_CONTEXT &cert);
    bool removeCertificate(const PCCERT_CONTEXT &cert);
    bool contains(const char *subjectName);
    CRYPT_INTEGER_BLOB &signData(const PCCERT_CONTEXT &cert, const HCRYPTPROV &hCryptProv, const std::string &file, const std::string &saveFile, const std::string &reason, const std::string &local);
    std::string signPDFtoMemory(const PCCERT_CONTEXT &cert, const HCRYPTPROV &hCryptProv, const std::string &file, const std::string &reason = "", const std::string &local = "");

    //DLL Functions
    CertOpenStoreType CertOpenStore;
    CertFindCertificateInStoreType CertFindCertificateInStore;
    CertEnumCertificatesInStoreType CertEnumCertificatesInStore;
    CertGetNameStringWType CertGetNameStringW;
    CertDeleteCertificateFromStoreType CertDeleteCertificateFromStore;
    CertCloseStoreType CertCloseStore;
    CryptSignMessageType CryptSignMessage;
    CryptVerifyMessageSignatureType CryptVerifyMessageSignature;
    CryptAcquireCertificatePrivateKeyType CryptAcquireCertificatePrivateKey;
    CryptAcquireContextWType CryptAcquireContextW;
    CryptGetUserKeyType CryptGetUserKey;
    CryptExportKeyType CryptExportKey;
    CryptCreateHashType CryptCreateHash;
    CryptHashDataType CryptHashData;
    CryptSignHashWType CryptSignHashW;
    CryptGetHashParamType CryptGetHashParam;
    CryptReleaseContextType CryptReleaseContext;
    CryptDestroyHashType CryptDestroyHash;
    CertGetIssuerCertificateFromStoreType CertGetIssuerCertificateFromStore;
private:
    HINSTANCE dllCrypt32lHandle;
    HINSTANCE dllCryptspHandle;
    HCERTSTORE hCertStore;
    PCCERT_CONTEXT pSignerCert;
    CRYPT_DATA_BLOB pSignedMessageBlob;
    CRYPT_DATA_BLOB pDecodedMessageBlob;
    std::deque<PCCERT_CONTEXT>  mSignerCert;
    std::deque<std::string> mSignerStringCert;
    CRYPT_INTEGER_BLOB retSignData;
    HCRYPTPROV retPK;
};

#endif // HPWINCRYPT_H
