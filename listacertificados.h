#ifndef LISTACERTIFICADOS_H
#define LISTACERTIFICADOS_H

#include <QDialog>

#include "hpwincrypt.h"

namespace Ui {
class ListaCertificados;
}

class ListaCertificados : public QDialog
{
    Q_OBJECT

public:
    explicit ListaCertificados(QWidget *parent = 0, HPWincrypt *crypt = 0, const QString &file = "", const QString &saveFile = "", const QString &reason = "", const QString &local = "");
    ~ListaCertificados();

private slots:
    void on_pushButton_clicked();

    void on_buttonBox_accepted();

    void on_listWidget_doubleClicked(const QModelIndex &index);

private:
    Ui::ListaCertificados *ui;
    HPWincrypt *crypt;
    QString file;
    QString saveFile;
    QString reason;
    QString local;

    void sign();
};

#endif // LISTACERTIFICADOS_H
