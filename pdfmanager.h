#ifndef PDFMANAGER_H
#define PDFMANAGER_H

#include <cstdio>
#include <string>
#include <vector>
#include <fstream>

struct PDFObj
{
    int id;
    int id2;
    int adr;
    std::string header;
    std::string headerByteArray;
    std::string stream;
    std::string data;
    std::string type;
    std::string subtype;
    std::string filter;
    std::string font;
    std::string encoding;
};

class PDFManager
{
public:
    PDFManager();
    PDFManager(std::string path);
    ~PDFManager();
    std::string getFile() const;
    std::ifstream *loadFile(const std::string &path);
    void closeFile();
    void loadPDF();
    std::string prepareSignatureData(const std::string &Reason = "", const std::string &Location = "");
    std::string insertSignatureData(const std::string &signedHash);
    std::string addSignedIncUpdate(const std::string &Reason = "", const std::string &Location = "");
    bool writeFile(std::string local = "", std::string data = "");
private:
    std::ifstream* pdffile;
    std::vector<PDFObj> objList;
    std::string fileStr;
};

#endif // PDFMANAGER_H
