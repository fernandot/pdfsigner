#include "pdfmanager.h"
#include "hpwincrypt.h"
#include <iostream>
#include <ios>
#include <regex>
#include "md5.h"
#include "io.h"

#include <QDir>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>

#define _USE_32BIT_TIME_T
#include <time.h>

void ReplaceAll(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
}

PDFManager::PDFManager(): pdffile(new std::ifstream())
{
}

PDFManager::PDFManager(std::string path): pdffile(new std::ifstream())
{
    loadFile(path);
    loadPDF();
}

PDFManager::~PDFManager()
{
    if(pdffile)
    {
        closeFile();
        delete pdffile;
        pdffile = 0;
    }

    objList.clear();
}

std::ifstream *PDFManager::loadFile(const std::string &path)
{
    pdffile->open(path,std::ios_base::binary);
    if(!pdffile->good())
    {
        return 0;
    }

    return pdffile;
}

void PDFManager::closeFile()
{
    if(pdffile->is_open())
        pdffile->close();
}

void PDFManager::loadPDF()
{
    if(pdffile->is_open())
    {
        std::stringstream ss;
        ss<<pdffile->rdbuf();
        fileStr = ss.str();

        try{
            std::regex reg("\\d+ \\d+ obj");
            std::smatch m;

            int position = 1;
            int lastLenght = 0;
            std::string::const_iterator searchStart( fileStr.cbegin() );
            while ( std::regex_search( searchStart, fileStr.cend(), m, reg ) )
            {
                position += m.position()+lastLenght;

                std::string temp = m.str(0);
                temp = temp.substr(0,temp.find(" "));

                std::string temp2 = m.str(0);
                temp2 = temp2.substr(temp2.find(" ")+1,temp2.size());
                temp2 = temp2.substr(0,temp2.find(" "));

                PDFObj obj;
                obj.id = atoi(temp.data());
                obj.id2 = atoi(temp2.data());

                obj.header = fileStr.substr(fileStr.find("<<",position-1)<fileStr.find("[",position-1)?fileStr.find("<<",position-1):fileStr.find("[",position-1),fileStr.size());
                obj.header = obj.header.substr(0,obj.header.find("stream")<obj.header.find("endobj")?obj.header.find("stream"):obj.header.find("endobj"));

                obj.adr = position-1;

                obj.stream = fileStr.substr(obj.adr,fileStr.size());
                obj.stream = obj.stream.substr(0,obj.stream.find("endobj"));
                if(obj.stream.find("stream")!=std::string::npos)
                {
                    obj.stream = obj.stream.substr(obj.stream.find("stream")+7,obj.stream.size());
                    obj.stream = obj.stream.substr(0,obj.stream.find("endstream"));
                }
                else obj.stream = "";

                if(obj.header.find("Type")!=std::string::npos)
                {
                    obj.type = obj.header.substr(obj.header.find("Type"),obj.header.size());
                    obj.type = obj.type.substr(obj.type.find("/")+1,obj.type.size());
                    obj.type = obj.type.substr(0,obj.type.find("/"));
                } else obj.type = "";

                if(obj.header.find("Subtype")!=std::string::npos)
                {
                    obj.subtype = obj.header.substr(obj.header.find("Subtype"),obj.header.size());
                    obj.subtype = obj.subtype.substr(obj.subtype.find("/")+1,obj.subtype.size());
                    obj.subtype = obj.subtype.substr(0,obj.subtype.find("/"));
                } else obj.subtype = "";

                objList.push_back(obj);

                searchStart += m.position()+m.length();
                lastLenght = m.length();
            }
        }
        catch (std::regex_error& e)
        {
            std::cerr << e.what() << ". Code: " << e.code() << std::endl;

            switch( e.code() )
            {
            case std::regex_constants::error_collate:
                std::cerr << "The expression contained an invalid collating element name.";
                break;
            case std::regex_constants::error_ctype:
                std::cerr << "The expression contained an invalid character class name.";
                break;
            case std::regex_constants::error_escape:
                std::cerr << "The expression contained an invalid escaped character, or a trailing escape.";
                break;
            case std::regex_constants::error_backref:
                std::cerr << "The expression contained an invalid back reference.";
                break;
            case std::regex_constants::error_brack:
                std::cerr << "The expression contained mismatched brackets ([ and ]).";
                break;
            case std::regex_constants::error_paren:
                std::cerr << "The expression contained mismatched parentheses (( and )).";
                break;
            case std::regex_constants::error_brace:
                std::cerr << "The expression contained mismatched braces ({ and }).";
                break;
            case std::regex_constants::error_badbrace:
                std::cerr << "The expression contained an invalid range between braces ({ and }).";
                break;
            case std::regex_constants::error_range:
                std::cerr << "The expression contained an invalid character range.";
                break;
            case std::regex_constants::error_space:
                std::cerr << "There was insufficient memory to convert the expression into a finite state machine.";
                break;
            case std::regex_constants::error_badrepeat:
                std::cerr << "The expression contained a repeat specifier (one of *?+{) that was not preceded by a valid regular expression.";
                break;
            case std::regex_constants::error_complexity:
                std::cerr << "The complexity of an attempted match against a regular expression exceeded a pre-set level.";
                break;
            case std::regex_constants::error_stack:
                std::cerr << "There was insufficient memory to determine whether the regular expression could match the specified character sequence.";
                break;
            default:
                std::cerr << "Undefined.";
                break;
            }
        }
        catch(std::exception e)
        {
            std::cout << e.what() << std::endl;
        }
    }
}

std::string PDFManager::prepareSignatureData(const std::string &Reason, const std::string &Location)
{
    std::string ret = "";
    int newObjCount = 6; ///Serão adicionados 6 objetos, por isso precisa aumentar a contagema

    if(fileStr.size()&&objList.size())
    {
        //Alter objects references
        int page = 0;
        int pages = 0;
        int catalog = 0;
        int info = 0;
        for(int i = objList.size();i>0;--i)
        {
            objList[i-1].id+=newObjCount;
            ReplaceAll(fileStr,std::string(" ")+std::to_string(i) + " " + std::to_string(0) + " R",std::string(" ")+std::to_string(i+newObjCount) + " " + std::to_string(0) + " R");
            ReplaceAll(fileStr,std::string("/")+std::to_string(i) + " " + std::to_string(0) + " R",std::string("/")+std::to_string(i+newObjCount) + " " + std::to_string(0) + " R");
            ReplaceAll(fileStr,std::string("[")+std::to_string(i) + " " + std::to_string(0) + " R",std::string("[")+std::to_string(i+newObjCount) + " " + std::to_string(0) + " R");
            ReplaceAll(fileStr,std::string("\n")+std::to_string(i) + " " + std::to_string(0) + " obj",std::string("\n")+std::to_string(i+newObjCount) + " " + std::to_string(0) + " obj");

            if(objList[i-1].type.compare("Page")==0)
                page = objList[i-1].id;
            else if(objList[i-1].type.compare("Pages")==0)
                pages = objList[i-1].id;
            else if(objList[i-1].type.compare("Catalog")==0)
                catalog = objList[i-1].id;
            else if(objList[i-1].header.size()&&objList[i-1].header.find("Producer")!=std::string::npos)
                info = i-1;
        }

        //data yyyyMMddhhmmss
        time_t rawtime;
        struct tm * timeinfo;
        char yyyyMMddhhmmss [80];

        time (&rawtime);
        timeinfo = localtime (&rawtime);
        std::strftime (yyyyMMddhhmmss,80,"%Y%m%d%H%M%S",timeinfo);


        //Insert signature object
        fileStr.insert(objList[0].adr,std::string("1 0 obj\n<</Type/Sig/Filter/Adobe.PPKLite/SubFilter/adbe.pkcs7.detached/Reason(")+Reason
                                  +std::string(")/Location(")+Location
                                  +std::string(")/ContactInfo()/M(D:")
                                  +std::string(yyyyMMddhhmmss)
                                  +std::string(timezone/3600<10?std::string("-0")+std::to_string(timezone/3600): std::to_string(timezone/3600))
                                  +std::string("\'00\')/ByteRange [0 "));


        //Insert signature objects
        fileStr.insert(objList[0].adr,std::string("2 0 obj\n<</FT/Sig/T(Assinador de PDF)/V 1 0 R/F 132/Type/Annot/Subtype/Widget/Rect[0 0 0 0]/AP<</N 3 0 R>>/P "+std::to_string(page)+ std::string(" 0 R/DR<<>>>>\nendobj\n")));

        //PDF visibility flag
        std::string hex_chars("0x78 0x9C 0x03 0x00 0x00 0x00 0x00 0x01");
        std::istringstream hex_chars_stream(hex_chars);
        unsigned char bytes[8];
        unsigned int hc;
        unsigned int c=0;
        std::string converted_hex="";
        while (hex_chars_stream >> std::hex >> hc)
        {
            bytes[c]=hc;
            c++;
            converted_hex+=(char)hc;
        }

        fileStr.insert(objList[0].adr,std::string("3 0 obj\n<</Type/XObject/Subtype/Form/Resources<<>>/BBox[0 0 0 0]/FormType 1/Matrix [1 0 0 1 0 0]/Length 8/Filter/FlateDecode>>stream\n")+converted_hex+std::string("\nendstream\nendobj\n"));

        fileStr.insert(objList[0].adr,std::string("4 0 obj\n<</Type/Font/BaseFont/Helvetica/Encoding/WinAnsiEncoding/Name/Helv/Subtype/Type1>>\nendobj\n"));

        fileStr.insert(objList[0].adr,std::string("5 0 obj\n<</Type/Font/BaseFont/ZapfDingbats/Name/ZaDb/Subtype/Type1>>\nendobj\n"));

        fileStr.insert(objList[0].adr,std::string("6 0 obj\n<</Type/Catalog/Pages ")+std::to_string(pages)+ std::string(" 0 R/AcroForm<</Fields[2 0 R]/DA(/Helv 0 Tf 0 g )/DR<</Font<</Helv 4 0 R/ZaDb 5 0 R>>>>/SigFlags 3>>>>\nendobj\n"));

        for(int i = objList.size()-1;i>=0;--i)
        {
            if(objList[i].id==page)
                fileStr.insert(fileStr.find_last_of(">>",fileStr.find(std::to_string(page)+" 0 obj")+std::to_string(page).size()+10+objList[i].header.size())-1,"/Annots[2 0 R]");
        }

        //insert byte range
        fileStr.insert(fileStr.find("ByteRange [0 ")+13,
                    std::to_string(fileStr.find("ByteRange [0 ")+100)
                    +std::string(" ")+std::to_string(16386+fileStr.find("ByteRange [0 ")+100)
                    +std::string(" ")
                    +std::string("").insert(0,90-(std::string("ByteRange [0 ")+std::to_string(fileStr.find("ByteRange [0 ")+123)+
                                                                std::string(" ")+std::to_string(16385+fileStr.find("ByteRange [0 ")+100)+
                                                              std::string(" ")).size(),'.')
                    +std::string("/Contents <")
                    +std::string("").insert(0,16384,'.')
                    +std::string(">>>\nendobj\n"));

        //Alter object's reference list
        for(const PDFObj &obj : objList) {
            int adr = fileStr.find('\n'+std::to_string(obj.id)+" "+std::to_string(obj.id2)+ " obj")+1;
            std::string objAdr = std::string(std::to_string(obj.adr).insert(0,10-std::to_string(obj.adr).size(),'0'));
            std::string newAdr = std::string(std::to_string(adr).insert(0,10-std::to_string(adr).size(),'0'));

            if(fileStr.find(objAdr)!=std::string::npos)
                fileStr.replace(fileStr.find(objAdr),objAdr.size(),newAdr);

        }

        //Altera a quantidade de obj na lista de referencia
        ReplaceAll(fileStr,std::string("xref\n0 "+std::to_string(objList.size()+1)),std::string("xref\n0 "+std::to_string(objList.size()+newObjCount+1)));
        //Altera a quantidade de obj na lista de referencia
        ReplaceAll(fileStr,std::string("xref\r\n0 "+std::to_string(objList.size()+1)),std::string("xref\r\n0 "+std::to_string(objList.size()+newObjCount+1)));

        //Insere o obj 1, 2, 3, 4, 5 e 6
        for(int c = newObjCount;c>0;--c)
        {
            int adr = fileStr.find(std::string("\n")+std::to_string(c)+" "+std::to_string(0)+ " obj")+1;
            fileStr.insert(fileStr.find("0000000000 65535 f")+20,(std::to_string(adr).insert(0,10-std::to_string(adr).size(),'0')+" 00000 n \n"));
        }

        //Altera o endereço do xref
        std::regex reg("\\d+");
        std::smatch m;
        std::string fileStrTemp = fileStr.substr(fileStr.find("startxref"));
        std::regex_search ( fileStrTemp, m, reg );

        if(fileStr.find(std::string(std::string("startxref\n")+m.str()))!=std::string::npos
           ||fileStr.find(std::string(std::string("startxref\r\n")+m.str()))!=std::string::npos)
        {
            ReplaceAll(fileStr,std::string(std::string("startxref\n")+m.str()),std::string("startxref\n")+std::to_string(fileStr.find("\n"+std::string("xref\n"))+1));
            ReplaceAll(fileStr,std::string(std::string("startxref\r\n")+m.str()),std::string("startxref\r\n")+std::to_string(fileStr.find("\r\n"+std::string("xref\r\n"))+1));
        }

        //Altera a linha de tamanho
        ReplaceAll(fileStr,std::string("<</Size "+std::to_string(objList.size()+1)),std::string("<</Size "+std::to_string(objList.size()+newObjCount+1)));

        //Altera o root
        ReplaceAll(fileStr,std::string("/Root "+std::to_string(catalog)),std::string("/Root "+std::to_string(6)));


        //Insere o tamanho do arquivo no byte range
        int fSize = fileStr.size()-(16386+fileStr.find("ByteRange [0 ")+100);
        fileStr.insert(fileStr.find("...."),std::to_string(fSize)+" ]");

        fileStr.replace(fileStr.find(std::to_string(fSize)+" ]")+std::string(std::to_string(fSize)+" ]").size(),
                     90-(std::string("ByteRange [0 ")+std::to_string(fileStr.find("ByteRange [0 ")+123)+
                                                                                     std::string(" ")+std::to_string(16386+fileStr.find("ByteRange [0 ")+100)+
                                                                                   std::string(" ")).size(),
                     std::string("").insert(0,90-(std::to_string(fSize)+" ]").size()-(std::string("ByteRange [0 ")+std::to_string(fileStr.find("ByteRange [0 ")+123)+
                                                    std::string(" ")+std::to_string(16386+fileStr.find("ByteRange [0 ")+100)+
                                                  std::string(" ")).size(),' '));


        //Altera a data de modificação.

        std::regex regMD("/ModDate\\(D:(\\d+\\-\\d+)");
        std::regex_search ( fileStr, m, regMD );

        if(m.size()==2)
        {
            if(info&&objList[info].header.find(m.str())!=std::string::npos)
                objList[info].header.replace(objList[info].header.find(m.str())+11,std::string(m[1]).size(),yyyyMMddhhmmss+std::string(timezone/3600<10?std::string("-0")+std::to_string(timezone/3600): std::to_string(timezone/3600)));

            fileStr.replace(fileStr.find(m.str())+11,std::string(m[1]).size(),yyyyMMddhhmmss+std::string(timezone/3600<10?std::string("-0")+std::to_string(timezone/3600): std::to_string(timezone/3600)));
        }

        //Change fileID
        std::regex regID("/ID \\[\\<[0-9a-f]+\\>(\\<[0-9a-f]+\\>)\\]", std::regex_constants::icase);
        std::regex_search ( fileStr, m, regID );

        if(m.size()==2)
        {
            char buff[PATH_MAX];
            getcwd( buff, PATH_MAX );
            std::string cwd( buff );

            ReplaceAll(cwd,"\\","/");
            ReplaceAll(fileStr,std::string(">")+std::string(m[1]),std::string("><")+MD5(yyyyMMddhhmmss+cwd+std::to_string(fileStr.size())+std::to_string(objList[info].id-newObjCount)+" "+std::to_string(objList[info].id2)+" obj\n"+objList[info].header).hexdigest()+std::string(">"));
        }

        ret = fileStr.substr(0,fileStr.find("/Contents <")+10);
        ret += fileStr.substr((fileStr.find("/Contents <")+12+16384),fileStr.size());
    }

    return ret;
}

std::string PDFManager::insertSignatureData(const std::string &signedHash)
{
    ReplaceAll(fileStr,std::string("").insert(0,16384,'.'),signedHash+std::string("").insert(0,16384-signedHash.size(),'0'));
    return fileStr;
}

std::string PDFManager::addSignedIncUpdate(const std::string &Reason, const std::string &Location)
{
    std::string ret = "";
    int newObjCount = 6; ///Serão adicionados 6 objetos, por isso precisa aumentar a contagema
    int last_fsize= fileStr.size();
    std::string signature_txt;

    if(fileStr.size()&&objList.size())
    {
        //Insert signature objects
        signature_txt.insert(0,std::string("2 0 obj\n<</FT/Sig/T(Assinador de PDF)/V 1 0 R/F 132/Type/Annot/Subtype/Widget/Rect[0 0 0 0]/AP<</N 3 0 R>>/P 0 0 R/DR<<>>>>\nendobj\n"));
        int obj2_size = signature_txt.size();

        ///Insert signature object
        //data yyyyMMddhhmmss
        time_t rawtime;
        struct tm * timeinfo;
        char yyyyMMddhhmmss [80];

        time (&rawtime);
        timeinfo = localtime (&rawtime);
        std::strftime (yyyyMMddhhmmss,80,"%Y%m%d%H%M%S",timeinfo);

        signature_txt.insert(0,std::string("1 0 obj\n<</Type/Sig/Filter/Adobe.PPKLite/SubFilter/adbe.pkcs7.detached/Reason(")+Reason
                                  +std::string(")/Location(")+Location
                                  +std::string(")/ContactInfo()/M(D:")
                                  +std::string(yyyyMMddhhmmss)
                                  +std::string(timezone/3600<10?std::string("-0")+std::to_string(timezone/3600): std::to_string(timezone/3600))
                                  +std::string("\'00\')/ByteRange [0 "));

        //insert byte range
        signature_txt.insert(signature_txt.find("ByteRange [0 ",last_fsize)+13,
        std::to_string(signature_txt.find("ByteRange [0 ",last_fsize)+100)
        +std::string(" ")+std::to_string(16386+signature_txt.find("ByteRange [0 ")+100)
        +std::string(" ")
        +std::string("").insert(0,90-(std::string("ByteRange [0 ")+std::to_string(signature_txt.find("ByteRange [0 ")+123)+
                                                    std::string(" ")+std::to_string(16385+signature_txt.find("ByteRange [0 ")+100)+
                                                  std::string(" ")).size(),'.')
        +std::string("/Contents <")
        +std::string("").insert(0,16384,'.')
        +std::string(">>>\nendobj\n"));

       /* //PDF visibility flag
        std::string hex_chars("0x78 0x9C 0x03 0x00 0x00 0x00 0x00 0x01");
        std::istringstream hex_chars_stream(hex_chars);
        unsigned char bytes[8];
        unsigned int hc;
        unsigned int c=0;
        std::string converted_hex="";
        while (hex_chars_stream >> std::hex >> hc)
        {
            bytes[c]=hc;
            c++;
            converted_hex+=(char)hc;
        }

        fileStr.insert(objList[0].adr,std::string("3 0 obj\n<</Type/XObject/Subtype/Form/Resources<<>>/BBox[0 0 0 0]/FormType 1/Matrix [1 0 0 1 0 0]/Length 8/Filter/FlateDecode>>stream\n")+converted_hex+std::string("\nendstream\nendobj\n"));

        fileStr.insert(objList[0].adr,std::string("4 0 obj\n<</Type/Font/BaseFont/Helvetica/Encoding/WinAnsiEncoding/Name/Helv/Subtype/Type1>>\nendobj\n"));

        fileStr.insert(objList[0].adr,std::string("5 0 obj\n<</Type/Font/BaseFont/ZapfDingbats/Name/ZaDb/Subtype/Type1>>\nendobj\n"));

        fileStr.insert(objList[0].adr,std::string("6 0 obj\n<</Type/Catalog/Pages ")+std::to_string(pages)+ std::string(" 0 R/AcroForm<</Fields[2 0 R]/DA(/Helv 0 Tf 0 g )/DR<</Font<</Helv 4 0 R/ZaDb 5 0 R>>>>/SigFlags 3>>>>\nendobj\n"));
        */
        int eof = fileStr.find_last_of("%%EOF");
        printf("EOF: %i\n",eof);

        std::regex reg("(<<\\/Size).*");
        std::smatch m;
        std::regex_search ( fileStr, m, reg );
        std::string linha_size = m.str().data();

        std::regex regD("\\d+");
        std::string fileStrTemp = fileStr.substr(fileStr.find("startxref"),fileStr.size());
        std::regex_search ( fileStrTemp, m, regD );
        std::string linha_size_final;
        linha_size_final = "/Prev ";
        linha_size_final += m.str().data();
        linha_size_final += ">>";
        linha_size = linha_size.replace(linha_size.find(">>"),2,linha_size_final.c_str());

        printf("%s",fileStrTemp.data());

        fileStr.insert(fileStr.size(), signature_txt +
                       std::string(
                        "xref\n"\
                        "0 2\n"\
                        "0000000000 65535 f\n")+
                        std::to_string(last_fsize).insert(0,10-std::to_string(last_fsize).size(),'0')+
                        std::string(" 00000 n \n")+
                        std::to_string(last_fsize+obj2_size).insert(0,10-std::to_string(last_fsize+obj2_size).size(),'0')+
                        std::string(" 00000 n \n")+
                        std::string("trailer\n")+
                        linha_size+
                        "\nstartxref\n"+
                        std::to_string(last_fsize+signature_txt.size())+
                        "\n%%EOF\n");
    }

    ret = fileStr;
    return ret;
}

bool PDFManager::writeFile(std::string local, std::string data)
{
    QFile file(local.data());
    if(file.exists())
    {
        if(QMessageBox::question(NULL,QObject::tr("Arquivo já existente"),QObject::tr("Deseja substituir o arquivo existente?"))==QMessageBox::No)
            return false;
    }

    std::ofstream ofs;
    ofs.open (local.data(), std::ofstream::binary);
    if (!ofs.is_open())
    {
        QMessageBox::warning(NULL,QObject::tr("Erro"),QObject::tr("Não foi possível salvar o arquivo no local informado."));
        return false;
    }

    ofs << data;
    ofs.close();

    if(QMessageBox::question(NULL,QObject::tr("Abrir o arquivo"),QObject::tr("Deseja abrir o arquivo salvo?"))==QMessageBox::Yes)
        QDesktopServices::openUrl(QUrl(QString("file:///")+local.data()));

    return true;
}

std::string PDFManager::getFile() const
{
    return fileStr;
}
