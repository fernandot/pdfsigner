#define UNICODE

#include "hpwincrypt.h"
#include <windows.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include "pdfmanager.h"

HPWincrypt::HPWincrypt()
{
    dllCrypt32lHandle = NULL;
    dllCryptspHandle = NULL;

    CertOpenStore = NULL;
    CertFindCertificateInStore = NULL;
    CertGetIssuerCertificateFromStore = NULL;
    CertEnumCertificatesInStore = NULL;
    CertDeleteCertificateFromStore = NULL;
    CertCloseStore = NULL;
    CryptSignMessage = NULL;
    CryptVerifyMessageSignature = NULL;
    CryptAcquireCertificatePrivateKey = NULL;
    CryptAcquireContextW = NULL;
    CertGetNameStringW = NULL;
    CryptGetUserKey = NULL;
    CryptExportKey = NULL;
    CryptCreateHash = NULL;
    CryptHashData = NULL;
    CryptSignHashW = NULL;
    CryptGetHashParam = NULL;
    CryptReleaseContext = NULL;
    CryptDestroyHash = NULL;

    hCertStore = NULL;
    pSignerCert = NULL;
}

HPWincrypt::~HPWincrypt()
{
    if(hCertStore)
        this->CertCloseStore(hCertStore, 0);

    FreeLibrary(dllCrypt32lHandle);
    FreeLibrary(dllCryptspHandle);
    free(pSignedMessageBlob.pbData);
}

bool HPWincrypt::Init()
{
    dllCrypt32lHandle = LoadLibrary(TEXT("C:\\Windows\\System32\\crypt32.dll"));
    dllCryptspHandle = LoadLibrary(TEXT("C:\\Windows\\System32\\cryptsp.dll"));

    // If the handle is valid, try to get the function address.
    if (!dllCrypt32lHandle||!dllCryptspHandle)
    {
        return false;
    }


    //Get pointer to our function using GetProcAddress:
    CertOpenStore = (CertOpenStoreType)GetProcAddress(dllCrypt32lHandle,"CertOpenStore");
    CertFindCertificateInStore = (CertFindCertificateInStoreType)GetProcAddress(dllCrypt32lHandle,"CertFindCertificateInStore");
    CertGetIssuerCertificateFromStore = (CertGetIssuerCertificateFromStoreType)GetProcAddress(dllCrypt32lHandle,"CertGetIssuerCertificateFromStore");
    CertEnumCertificatesInStore = (CertEnumCertificatesInStoreType)GetProcAddress(dllCrypt32lHandle,"CertEnumCertificatesInStore");
    CertGetNameStringW = (CertGetNameStringWType)GetProcAddress(dllCrypt32lHandle,"CertGetNameStringW");
    CertDeleteCertificateFromStore = (CertDeleteCertificateFromStoreType)GetProcAddress(dllCrypt32lHandle,"CertDeleteCertificateFromStore");
    CertCloseStore = (CertCloseStoreType)GetProcAddress(dllCrypt32lHandle,"CertCloseStore");
    CryptSignMessage = (CryptSignMessageType)GetProcAddress(dllCrypt32lHandle,"CryptSignMessage");
    CryptVerifyMessageSignature = (CryptVerifyMessageSignatureType)GetProcAddress(dllCrypt32lHandle,"CryptVerifyMessageSignature");
    CryptAcquireCertificatePrivateKey = (CryptAcquireCertificatePrivateKeyType)GetProcAddress(dllCrypt32lHandle,"CryptAcquireCertificatePrivateKey");
    CryptAcquireContextW = (CryptAcquireContextWType)GetProcAddress(dllCryptspHandle,"CryptAcquireContextW");
    CryptGetUserKey = (CryptGetUserKeyType)GetProcAddress(dllCryptspHandle,"CryptGetUserKey");
    CryptExportKey = (CryptExportKeyType)GetProcAddress(dllCryptspHandle,"CryptExportKey");
    CryptCreateHash = (CryptCreateHashType)GetProcAddress(dllCryptspHandle,"CryptCreateHash");
    CryptHashData = (CryptHashDataType)GetProcAddress(dllCryptspHandle,"CryptHashData");
    CryptSignHashW = (CryptSignHashWType)GetProcAddress(dllCryptspHandle,"CryptSignHashW");
    CryptGetHashParam = (CryptGetHashParamType)GetProcAddress(dllCryptspHandle,"CryptGetHashParam");
    CryptReleaseContext = (CryptReleaseContextType)GetProcAddress(dllCryptspHandle,"CryptReleaseContext");
    CryptDestroyHash = (CryptDestroyHashType)GetProcAddress(dllCryptspHandle,"CryptDestroyHash");

    return (CertOpenStore &&
            CertFindCertificateInStore &&
            CertGetIssuerCertificateFromStore &&
            CertEnumCertificatesInStore &&
            CertDeleteCertificateFromStore &&
            CertGetNameStringW &&
            CertCloseStore &&
            CryptSignMessage &&
            CryptVerifyMessageSignature &&
            CryptAcquireCertificatePrivateKey &&
            CryptAcquireContextW &&
            CryptGetUserKey &&
            CryptExportKey &&
            CryptCreateHash &&
            CryptHashData &&
            CryptSignHashW &&
            CryptGetHashParam &&
            CryptReleaseContext &&
            CryptDestroyHash);
}

HCERTSTORE HPWincrypt::getCertStore(const wchar_t *certName)
{
    if(!hCertStore)
    {
        hCertStore = this->CertOpenStore(
           CERT_STORE_PROV_SYSTEM,   // the store provider type
           0,                        // the encoding type is not needed
           NULL,                     // use the default HCRYPTPROV
           CERT_SYSTEM_STORE_CURRENT_USER,
                                     // set the store location in a
                                     // registry location
           certName    // the store name as a Unicode string
           );
    }
    return hCertStore;
}

std::deque<PCCERT_CONTEXT> &HPWincrypt::getSignerList()
{
    mSignerCert.clear();
    // If call was successful, close hSystStore; otherwise,
    // call print an error message.
   hCertStore = getCertStore(L"My");
    if(hCertStore)
    {
        pSignerCert = NULL;
        while(pSignerCert = this->CertEnumCertificatesInStore(
          hCertStore,
          pSignerCert))
        {
            if(!pSignerCert->pCertInfo->Subject.cbData) break;

            if(pSignerCert->pCertInfo->Subject.cbData == pSignerCert->pCertInfo->Issuer.cbData) continue;

            mSignerCert.push_back(pSignerCert);

            WCHAR name[128];
            this->CertGetNameStringW(pSignerCert,CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, name, sizeof(name));

            std::wstring ws = name;
            std::string s(ws.begin(),ws.end());
        }
    }

    return mSignerCert;
}

std::deque<std::string> &HPWincrypt::getSignerStringList()
{
    mSignerStringCert.clear();
    // If call was successful, close hSystStore; otherwise,
    // call print an error message.
   hCertStore = getCertStore(L"My");
    if(hCertStore)
    {
        pSignerCert = NULL;
        while(pSignerCert = this->CertEnumCertificatesInStore(
          hCertStore,
          pSignerCert))
        {
            if(!pSignerCert->pCertInfo->Subject.cbData) break;

            if(pSignerCert->pCertInfo->Subject.cbData == pSignerCert->pCertInfo->Issuer.cbData) continue;

            WCHAR name[128];
            this->CertGetNameStringW(pSignerCert,CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, name, sizeof(name));

            std::wstring ws = name;
            std::string s(ws.begin(),ws.end());

            mSignerStringCert.push_back(s.data());
        }
    }

    return mSignerStringCert;
}

PCCERT_CONTEXT &HPWincrypt::getSignerCertificate(const wchar_t *subjectName)
{
    hCertStore = getCertStore(L"My");
    pSignerCert = this->CertFindCertificateInStore(
           hCertStore,
           MY_ENCODING_TYPE,
           0,
           CERT_FIND_SUBJECT_STR,
           subjectName,
           NULL);

    return pSignerCert;
}

CRYPT_DATA_BLOB &HPWincrypt::getSignedMessage(PCCERT_CONTEXT signer, BYTE *msg, int msgLen)
{

    pSignedMessageBlob.cbData = 0;
    pSignedMessageBlob.pbData = NULL;

    CRYPT_SIGN_MESSAGE_PARA  SigParams;
    BYTE* pbMessage = msg;
        DWORD cbMessage;
    DWORD cbSignedMessageBlob;
    BYTE  *pbSignedMessageBlob = NULL;

    // Calculate the size of message. To include the
    // terminating null character, the length is one more byte
    // than the length returned by the strlen function.
    cbMessage = msgLen?msgLen:(lstrlen((TCHAR*) pbMessage) + 1) * sizeof(TCHAR);
    const BYTE* MessageArray[] = {pbMessage};
    DWORD_PTR MessageSizeArray[1];
    MessageSizeArray[0] = cbMessage;
    // Initialize the signature structure.
    SigParams.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
    SigParams.dwMsgEncodingType = MY_ENCODING_TYPE;
    SigParams.pSigningCert = signer;
    //SigParams.HashAlgorithm.pszObjId = szOID_RSA_SHA1RSA;
    SigParams.HashAlgorithm.pszObjId = "1.2.840.113549.1.1.11";
    SigParams.HashAlgorithm.Parameters.cbData = NULL;
    SigParams.cMsgCert = 1;
    SigParams.rgpMsgCert = &signer;
    SigParams.cAuthAttr = 0;
    SigParams.dwInnerContentType = 0;
    SigParams.cMsgCrl = 0;
    SigParams.cUnauthAttr = 0;
    SigParams.dwFlags = NULL;
    SigParams.pvHashAuxInfo = NULL;
    SigParams.rgAuthAttr = NULL;
    // First, get the size of the signed BLOB.
    if(this->CryptSignMessage(
        &SigParams,
        TRUE,
        1,
        MessageArray,
        MessageSizeArray,
        NULL,
        &cbSignedMessageBlob))
    {
        // Allocate memory for the signed BLOB.
        pbSignedMessageBlob = (BYTE*)malloc(cbSignedMessageBlob);

        // Get the signed message BLOB.
        if(this->CryptSignMessage(
              &SigParams,
              TRUE,
              1,
              MessageArray,
              MessageSizeArray,
              pbSignedMessageBlob,
              &cbSignedMessageBlob))
        {
            pSignedMessageBlob.cbData = cbSignedMessageBlob;
            pSignedMessageBlob.pbData = pbSignedMessageBlob;

        }
    }

    return pSignedMessageBlob;
}

CRYPT_DATA_BLOB &HPWincrypt::getVerifiedSignedMessage(CRYPT_DATA_BLOB &pSignedMessage)
{
    DWORD cbDecodedMessageBlob;
    BYTE *pbDecodedMessageBlob = NULL;
    CRYPT_VERIFY_MESSAGE_PARA VerifyParams;

    // Initialize the output.
    pDecodedMessageBlob.cbData = 0;
    pDecodedMessageBlob.pbData = NULL;

    // Initialize the VerifyParams data structure.
    VerifyParams.cbSize = sizeof(CRYPT_VERIFY_MESSAGE_PARA);
    VerifyParams.dwMsgAndCertEncodingType = MY_ENCODING_TYPE;
    VerifyParams.hCryptProv = 0;
    VerifyParams.pfnGetSignerCertificate = NULL;
    VerifyParams.pvGetArg = NULL;

    if(this->CryptVerifyMessageSignature(
        &VerifyParams,
        0,
        pSignedMessage.pbData,
        pSignedMessage.cbData,
        NULL,
        &cbDecodedMessageBlob,
        NULL))
    {
        //---------------------------------------------------------------
        //   Allocate memory for the decoded message.
        pbDecodedMessageBlob = (BYTE*)malloc(cbDecodedMessageBlob);

        //---------------------------------------------------------------
        // Call CryptVerifyMessageSignature again to verify the signature
        // and, if successful, copy the decoded message into the buffer.
        // This will validate the signature against the certificate in
        // the local store.
        if(this->CryptVerifyMessageSignature(
            &VerifyParams,
            0,
            pSignedMessage.pbData,
            pSignedMessage.cbData,
            pbDecodedMessageBlob,
            &cbDecodedMessageBlob,
            NULL))
        {
            pDecodedMessageBlob.cbData = cbDecodedMessageBlob;
            pDecodedMessageBlob.pbData = pbDecodedMessageBlob;
        }
    }

    return pDecodedMessageBlob;
}


HCRYPTPROV_OR_NCRYPT_KEY_HANDLE &HPWincrypt::getPrivateKey(PCCERT_CONTEXT &cert)
{
    retPK = NULL;
    DWORD dwKeySpec;
    HCRYPTKEY hKey;
    DWORD pubkeyLen;
    BYTE *pubkey;
    if(( this->CryptAcquireCertificatePrivateKey(
            cert,
            0,
            NULL,
            &retPK,
            &dwKeySpec,
            NULL)))
    {
        if(this->CryptGetUserKey(retPK,AT_SIGNATURE,&hKey))
        {
            CryptExportKey(hKey, NULL, PRIVATEKEYBLOB, 0, NULL, &pubkeyLen);
            pubkey = (BYTE*)malloc(pubkeyLen);
            CryptExportKey(hKey, NULL, PRIVATEKEYBLOB, 0, pubkey, &pubkeyLen);
        }
    }

    return retPK;
}

bool HPWincrypt::removeCertificate(const PCCERT_CONTEXT &cert)
{
    return this->CertDeleteCertificateFromStore(cert);
}

bool HPWincrypt::contains(const char *subjectName)
{
    bool contain = false;

    std::deque<std::string> slist = this->getSignerStringList();
    for(int c = 0; c<slist.size();c++) {
            if(0 == slist.at(c).compare(subjectName))
            {
                contain = true;
                break;
            }
        }

    return contain;
}

CRYPT_INTEGER_BLOB &HPWincrypt::signData(const PCCERT_CONTEXT &cert, const HCRYPTPROV &hCryptProv, const std::string &file, const std::string &saveFile, const std::string &reason, const std::string &local)
{
    HCRYPTHASH hHash;
    PDFManager *pdf = new PDFManager(file.data());
    std::string prep;

    if(this->CryptCreateHash(hCryptProv,CALG_SHA1, 0, 0,&hHash))
    {
        prep=pdf->prepareSignatureData(reason.data(),local.data());
        pdf->addSignedIncUpdate(reason.data(),local.data());

        BYTE bt[prep.size()];
        for(int i=0;i<prep.size();++i)
            bt[i]=(BYTE)prep[i];

        retSignData =  getSignedMessage(cert,bt,prep.size());
        if(retSignData.cbData)
        {
            unsigned char HEX[(int)retSignData.cbData];
            memcpy(HEX,(char*)retSignData.pbData,(int)retSignData.cbData);
            std::stringstream signedHEX;
            for (int i = 0; i < (int)retSignData.cbData; i++)
            {
                signedHEX << std::hex << std::setfill('0');
                signedHEX << std::setw(2) << static_cast<unsigned>(HEX[i]);
            }

            pdf->writeFile(saveFile.data(),pdf->insertSignatureData(signedHEX.str()));
        }
    }

    return retSignData;
}

std::string HPWincrypt::signPDFtoMemory(const PCCERT_CONTEXT &cert, const HCRYPTPROV &hCryptProv, const std::string &file, const std::string &reason, const std::string &local)
{
    HCRYPTHASH hHash;
    PDFManager *pdf = new PDFManager(file.data());
    std::string prep;

    if(this->CryptCreateHash(hCryptProv,CALG_SHA1, 0, 0,&hHash))
    {
        prep=pdf->prepareSignatureData(reason.data(),local.data());

        BYTE bt[prep.size()];
        for(int i=0;i<prep.size();++i)
            bt[i]=(BYTE)prep[i];

        CRYPT_DATA_BLOB signedMessage = getSignedMessage(cert,bt,prep.size());
        if(signedMessage.cbData)
        {
            unsigned char HEX[(int)signedMessage.cbData];
            memcpy(HEX,(char*)signedMessage.pbData,(int)signedMessage.cbData);
            std::stringstream signedHEX;
            for (int i = 0; i < (int)signedMessage.cbData; i++)
                {
                    signedHEX << std::hex << std::setfill('0');
                    signedHEX << std::setw(2) << static_cast<unsigned>(HEX[i]);
                }
            std::string hexString = signedHEX.str();
            return pdf->insertSignatureData(hexString);
        }
    }
    return "";
}

