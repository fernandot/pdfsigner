#-------------------------------------------------
#
# Project created by QtCreator 2015-07-01T13:49:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PDFSigner
TEMPLATE = app

ICON = PDF.ico

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    hpwincrypt.cpp \
    pdfmanager.cpp \
    listacertificados.cpp \
    md5.cpp

HEADERS  += mainwindow.h \
    hpwincrypt.h \
    pdfmanager.h \
    listacertificados.h \
    md5.h


FORMS    += mainwindow.ui \
    listacertificados.ui

OTHER_FILES += \
    icone.rc
