#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "listacertificados.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QMimeData>

#include "hpwincrypt.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    crypt = new HPWincrypt();

    if(!crypt->Init())
    {
        QMessageBox::critical(this,"Erro","Erro ao iniciar a criptografia, feche o aplicativo e tente novamente.");
    }

    //Import PDF's sent as arguments
    QStringList uris = QCoreApplication::arguments();
    for(int c=uris.size()-1;c>=0;--c)
    {
        QString uri = uris.at(c);
        uri=uri.remove(0,8);
        uri.chop(1);

        if(uri.endsWith(".pdf",Qt::CaseInsensitive))
        {
            uri.replace("%20"," ");
            uris.replace(c,uri);
        }
        else
        {
            uris.removeAt(c);
        }
    }
    if(uris.size())
    {
        ui->lineEdit->setText(uris[0]);
        ui->lineEdit_2->setText(uris[0].left(uris[0].lastIndexOf("."))+" assinado.pdf");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dropEvent(QDropEvent *event)
{
    QString mime = event->mimeData()->data("text/uri-list");
    QStringList uris = mime.split("\n",QString::SkipEmptyParts);

    for(int c=uris.size()-1;c>=0;--c)
    {
        QString uri = uris.at(c);
        uri=uri.remove(0,8);
        uri.chop(1);

        if(uri.endsWith(".pdf",Qt::CaseInsensitive))
        {
            uri.replace("%20"," ");
            uris.replace(c,uri);
        }
        else
        {
            uris.removeAt(c);
        }
    }
    if(uris.size())
    {
        ui->lineEdit->setText(uris[0]);
        ui->lineEdit_2->setText(uris[0].left(uris[0].lastIndexOf("."))+" assinado.pdf");
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void MainWindow::on_pushButton_clicked()
{
    ListaCertificados *lc = new ListaCertificados(this, crypt,ui->lineEdit->text(), ui->lineEdit_2->text()
                                                  ,"","");
    lc->show();
}

void MainWindow::on_toolButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Selecione um PDF"),
                                                     "",
                                                     tr("PDF (*.pdf)"));
    if(fileName.size())
    {
        ui->lineEdit->setText(fileName);
        ui->lineEdit_2->setText(fileName.left(fileName.lastIndexOf("."))+" assinado.pdf");
    }
}

void MainWindow::on_toolButton_2_clicked()
{
    if(!ui->lineEdit->text().size())
    {
        QMessageBox::warning(this,"Selecione o PDF","Selecione o arquivo primeiro.");
        return;
    }

    QString folderName = QFileDialog::getExistingDirectory(this, tr("Selecione um PDF"),
                                                     QDir(ui->lineEdit->text()).absolutePath(),
                                                           QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(folderName.size())
    {
        ui->lineEdit_2->setText(folderName+ "/" + QFileInfo(ui->lineEdit->text()).fileName().left(QFileInfo(ui->lineEdit->text()).fileName().lastIndexOf("."))+" assinado.pdf");
    }
}
